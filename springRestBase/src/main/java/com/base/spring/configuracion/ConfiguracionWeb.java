package com.base.spring.configuracion;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan({"com.base.spring"})
public class ConfiguracionWeb {

	@Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }
	
	@Autowired
	@Bean(name = "dataSource")
	public DataSource getDataSource(){
		JndiTemplate jndiTemplate = new JndiTemplate();
		DataSource dataSource = null;
		try {
//			dataSource = (DataSource) jndiTemplate.lookup("java:/comp/env/jdbc/Oracle");
			dataSource = (DataSource) jndiTemplate.lookup("/jdbc/Oracle");
		} catch (NamingException e) {
		}
		return dataSource;
	}
}
