package com.base.spring.sistema.beans;

import java.security.SecureRandom;

public class BaseBean {
	private SecureRandom versionador = new SecureRandom();
	private	double	version;
	
	public BaseBean() {
		version = versionador.nextDouble();
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}
}
